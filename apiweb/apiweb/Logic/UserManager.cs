﻿using System;
using System.Collections.Generic;

namespace Logic
{
    public class UserManager
    {
        public List<User> Users { get; set; }
        public UserManager()
        {
            Users = new List<User>()
            {
               new User(){Name = "Adrian",Ci=8032729},
               new User(){Name = "Sofia",Ci=8032710},
            };
        }


        public List<User> GetUsers()
        {
            return Users;
        }
       
        public User PostUsers(User user)
        {
            Users.Add(user);
            return user;
        }

        public User PutUsers(User user)
        {
            throw new NotImplementedException();
        }
    
        public User DeleteUsers(int Ci)
        {
            throw new NotImplementedException();
        }
    }
}
